<div style="width:100%;text-align:center;">
<a> <img src=https://securityvisit.com/wp-content/uploads/2021/07/Apps-Built-Better-Why-DevSecOps-is-Your-Security-Teams-Silver.png> </a>
</div>

# README #

https://gitlab.com/Dmitry.Plikus/devsecops.git
## DevSecOps
###### [More information about what DevSecOps is here](https://www.avisto.com/en/devsecops-engineer/ "More information about what DevSecOps is here")
--------------------------------------------------------------------------------------------------------
##### This example step by step through actions required to change ntp server to ntp.se using VagrantBox and VirtualBox to RHEL8. Server configuration and setup will be done using Ansible.
##### - [More information about Ansible](https://www.ansible.com/ "More information about Ansible")
##### - [More information about VagrantBox](https://www.vagrantup.com/ "More information about VagrantBox")
##### - [More information about VirtualBox](https://www.virtualbox.org/ "More information about VirtualBox")
##### - [More information about RHEL8](https://www.redhat.com/en/enterprise-linux-8 "More information about RHEL8")
--------------------------------------------------------------------------------------------------------
### Task:
##### Change ntp to ntp.se with Ansible in an RHEL 8 virtual server named ntp.edu.tentixo.com via VagrantBox on VirtualBox - connection should be done with computer name, not IP.

--------------------------------------------------------------------------------------------------------
### Objective of the project
##### The purpose of the work is to acquire practical skills in working with Ansible, VagrantBox, VirtualBox, RHEL8, as well as an example of automating the process of setting up an ntp server
--------------------------------------------------------------------------------------------------------
   ## How to use?
   #####  This project work on Ubuntu 20.04! On other Linux families has not been tested!!!
   ###   0. Install vagrant and VirtualBox:
            ```
            $ sudo apt update
            $ sudo apt install virtualbox  
            $ curl -O https://releases.hashicorp.com/vagrant/2.2.19/vagrant_2.2.19_x86_64.deb   
            $ sudo apt install ./vagrant_2.2.9_x86_64.deb
            ```
   ###   1. Create project folder:
            ```
            $ mkdir Project_1
            $ cd ./Project_1/
            ```

   ###   2. Create your virtualenv:
            ```
            # Clone Git repository with my project
            $ git clone https://gitlab.com/Dmitry.Plikus/devsecops.git 
            $ cd ./devsecops/
            $ python3 -m venv devsecops
            $ source devsecops/bin/activate
            # Install packages
            $ pip install -r requirements.txt
            ```
   ###   3. Vagrant build:
            ```
            $ vagrant up
            ```
   ###   4. Connect to VM:
            ```
            $ vagrant ssh ntp.edu.tentixo.com
            ```
   ###   5. Checking the work of ntp servers:
            ```
            $ chronyc sources
            $ chronyc tracking
            ```
--------------------------------------------------------------------------------------------------------

   # About files #
   ##  1. Vagrantfile:
   ### <p><a href="https://www.vagrantup.com/docs/vagrantfile">link on official Docs for Vagrant</a></p>
            ```
            Vagrant.configure("2") do |config|
                  ##### VM definition #####   
                  config.vm.define "ntp.edu.tentixo.com" do |config|          
                  config.vm.hostname = "ntp.edu.tentixo.com"                  
                  config.vm.box = "generic/rhel8"                             
                  config.vm.network "private_network", ip: "172.28.128.22"    
                  config.vm.box_check_update = false                         

                  config.vm.provision :ansible do |ansible|
                        ansible.playbook = "provision.yaml"                   
                        ansible.inventory_path = "inventory"                  
                  end

                  config.vm.provider "virtualbox" do |v|                      
                        v.memory = 2048                                       
                        v.cpus = 2                                            
                        end
                  end
            end
            ```

  **Description:**
  - config.vm.define - Description of server name and config name
  - config.vm.hostname - Hostname definition 
  - config.vm.box - Specifying the image that the vagrant will use and deploy
  - config.vm.network - Description of the network type, specifying the ip of the machine
  - config.vm.provision - Description of provision, its type and config name
  - config.vm.provider - Provider selection and its config

   ###   2. Provision.yaml file:
            ```
            ---
          - hosts: ntp
            become: true
            tasks:
              - name: Update OS
                package:
                  name: '*'
                  state: latest

              - name: Make sure Chrony is started up
                service: 
                  name: chronyd 
                  state: started 
                  enabled: yes

              - name: Replace line in config file
                lineinfile:
                  path: /etc/chrony.conf
                  regexp: '^pool(.*)'
                  line: 'server sth1.ntp.se \n server sth2.ntp.se'
                  backrefs: yes       

              - name: Restart chronyd.service
                shell: systemctl restart chronyd.service
            ```

      
   ###   3. About inventory file:
            In the file, I indicated the name of the machine, its ip

            You can read more information about inventory file here:
            https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html


   ###   4. About requirements.txt:
            
            Good article about requirements.txt:
            https://blog.sedicomm.com/2021/06/29/chto-takoe-virtualenv-v-python-i-kak-ego-ispolzovat/


            